<!--
  ~ JBoss, Home of Professional Open Source.
  ~ Copyright (c) 2011, Red Hat, Inc., and individual contributors
  ~ as indicated by the @author tags. See the copyright.txt file in the
  ~ distribution for a full listing of individual contributors.
  ~
  ~ This is free software; you can redistribute it and/or modify it
  ~ under the terms of the GNU Lesser General Public License as
  ~ published by the Free Software Foundation; either version 2.1 of
  ~ the License, or (at your option) any later version.
  ~
  ~ This software is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  ~ Lesser General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Lesser General Public
  ~ License along with this software; if not, write to the Free
  ~ Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
  ~ 02110-1301 USA, or see the FSF site: http://www.fsf.org.
  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 
<html>
<head>
    <title>Welcome to ${properties.displayName}</title>

    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <link rel="shortcut icon" href="${resourcesPath}/img/favicon.ico" />

    <#if properties.stylesCommon?has_content>
        <#list properties.stylesCommon?split(' ') as style>
            <link href="${resourcesCommonPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <style>
        body {
            background: none;
            font-size: 14px;
        }
        .epicur-bg {
            opacity: 0.1;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: auto;
        }
        a.application-tile {
            text-decoration: none;
            display:block;
        }

        a.application-tile:hover {
            transform: scale(1.1);
        }

        .welcome-header {
	        margin-bottom: 20px;
        }
        .welcome-message {
	        margin-top: 0px;
            margin-bottom: 20px;
        }
        .welcome-header img {
            width: 210px;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>
<img
    class="epicur-bg" style="display:none"
    src="https://epicur.edu.eu/wp-content/themes/business-epic-child/assets/imgs/Epicur-Map2-230404-MW_Epicur-Map-sans-cartouche.svg"
    alt=""
  >
<div class="container-fluid" data-application-list-service-url="${properties.applicationListServiceUrl}">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
      <div class="welcome-header">
        <img src="https://register.epicur.auth.gr/assets/img/epicur-logo.png" alt="${productName}" border="0" />
        <h1>Welcome to <strong>EPICUR Virtual Inter-University Campus</strong></h1>
      </div>
      <div class="text-justify welcome-message">
        The EPICUR Inter-University Campus is a central online gateway for supporting flexible virtual, physical and hybrid mobilities for all students and staff across the alliance.
        One of its unique features is its fully digitalized, paper-free process for student admissions
        that is supported by a secure, scalable and transparent solution based on free and open-source software. Furthermore, EIUC offers innovative functionality to monitor mobilities, issue and recognize certifications (including ECTS and micro-credentials). 
      </div>
      <div class="row">
        <div class="application-list">
        </div>
        <script id="applicationTemplate" type="text/html">
            <div class="col-xs-12 col-sm-12 col-md-4">
                <a class="application-tile" href="<%=url%>">
                <div class="card-pf h-l">
                    <h2><%=name%></h2>
                    <div>
                        <img style="width:100% !important" style="height:128px" src="<%=image%>">
                    </div>
                </div>
                </a>
            </div>
        </script>
      </div>
    </div>
  </div>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/ejs@3.1.9/ejs.min.js" integrity="sha256-0rgfHeWzCumX4KhMei2fyXeBSjZLcoSsUNpaXX5dYVI=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function() {
        const applicationListServiceUrl = $('.container-fluid').attr('data-application-list-service-url');
        if (applicationListServiceUrl) {
            // get template string
            const templateString = $('#applicationTemplate').text();
            const listElement = $('.application-list');
            $.get(applicationListServiceUrl, function( data ) {
                const applications = data.applications;
                applications.forEach((application) => {
                   const applicationElement = ejs.render(templateString, application);
                   listElement.append(applicationElement);
                });
            });
        }
    });
</script>
</html>
